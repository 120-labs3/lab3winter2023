import java.util.Scanner;

public class ApplianceStore{
	
	public static void main(String[] args) throws InterruptedException{
		
		Scanner input = new Scanner(System.in);
		RiceCooker[] cooker = new RiceCooker[4];
		
		for(int i = 0;i<cooker.length;i++){
			
			cooker[i] = new RiceCooker();
			
			System.out.println("Enter the brand of your rice Cooker");
			cooker[i].brand = input.nextLine();
			System.out.println("Enter how much rice your rice Cooker can contain");
			cooker[i].capacityOfRice = Integer.parseInt(input.nextLine());
			System.out.println("Enter the name of the tune your rice Cooker plays after it has finish doing its job.");
			cooker[i].tune = input.nextLine();
		}
		
		System.out.println("the brand is: " + cooker[cooker.length - 1].brand);
		System.out.println("the tune is: " + cooker[cooker.length - 1].tune);
		System.out.println("the capacity of Rice is: " + cooker[cooker.length - 1].capacityOfRice);
		
		System.out.print("The rice cooker has played: ");
		cooker[1].sing();
		System.out.println("The rice Cooker has cooked : " + cooker[1].riceCooked() + " grams of rice");
	}
}