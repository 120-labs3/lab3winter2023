public class RiceCooker{
	
	public String brand;
	public int capacityOfRice;
	public String tune;
	
	public void sing() throws InterruptedException {
		
		int x = 0;
		for(int i = 5; i>0;i--){
			
			Thread.sleep(1000);
			System.out.println(i + "Seconds left");
			
			}
		
		System.out.print(this.tune);
	}
	
	public int riceCooked(){
		return this.capacityOfRice;
	}
}	